class Product {
  int id;
  int categoryId;
  String productName;
  String quantityPerUnit;
  double unitPrice;
  int unitsInStock;

  Product(this.id, this.categoryId, this.productName, this.quantityPerUnit,
      this.unitPrice, this.unitsInStock);

  Product.fromJson(Map json) {
    this.id = int.tryParse(json["id"]);
    this.categoryId = int.tryParse(json["categoryId"]);
    this.productName = json["productName"];
    this.quantityPerUnit = json["quantityPerUnit"];
    this.unitPrice = double.tryParse(json["unitPrice"]);
    this.unitsInStock = json["unitsInStock"];
  }

  Map toJson() {
    Map productJsonFormat = {
      "id": this.id,
      "categoryId": this.categoryId,
      "productName": this.productName,
      "quantityPerUnit": this.quantityPerUnit,
      "unitPrice": this.unitPrice,
      "unitsInStock": this.unitsInStock
    };

    return productJsonFormat;
  }
}