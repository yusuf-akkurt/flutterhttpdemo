class Category {
  int id;
  String categoryName;
  String seoUrl;

  Category(this.id, this.categoryName, this.seoUrl);

  Category.fromJson(Map json) {
    this.id = json["id"];
    this.categoryName = json["categoryName"];
    this.seoUrl = json["seoUrl"];
  }

  Map toJson() {
    Map categoryJsonFormat = {
      "id": this.id,
      "categoryName": this.categoryName,
      "seoUrl": this.seoUrl
    };

    return categoryJsonFormat;
  }
}
