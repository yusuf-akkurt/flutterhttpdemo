import 'package:http/http.dart' as http;

class CategoryApi{
  static String apiUrl = "10.0.2.2:3000";

  static Future getCategories() {
    return http.get(Uri.http(apiUrl, "categories"));
  }
}