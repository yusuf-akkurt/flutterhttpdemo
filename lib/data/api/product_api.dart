import 'dart:convert';

import 'package:http/http.dart' as http;

class ProductApi {
  static String apiUrl = "http://10.0.2.2:3000/products/";

  static Future getProducts() {
    return http.get(jsonDecode(apiUrl));
  }

  static Future getProductsByCategoryId(int categoryId) {
    return http.get(jsonDecode(apiUrl + "?categoryId=$categoryId"));
  }
}
